import beans.Building;
import beans.Container;
import job.Controller;
import job.Generator;
import beans.Passenger;
import job.TransportationTask;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runner {

    public static void main(String args[]) {
        Generator generator = new Generator("config");
        Building building = new Building(generator);
        Controller controller = new Controller(building);
        ExecutorService pool = Executors.newFixedThreadPool(generator.getPassengersNumber());
        for (Passenger pas : building.getPassengersList()) {
            TransportationTask task = new TransportationTask(pas, building, controller);
            pool.execute(task);
        }
        pool.shutdown();
        Thread controllerThread = new Thread(controller);
        controllerThread.start();

    }
}
