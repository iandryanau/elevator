package beans;

import enums.Direction;

import java.util.ArrayList;
import java.util.List;

import enums.TransportationActions;
import org.apache.log4j.Logger;

public class ElevatorContainer {

    private List<Passenger> passengersList;
    private int currentStory;
    private Direction direction;
    private int elevatorCapacity;
    private int storiesNumber;

    private Logger logger = Logger.getLogger(ElevatorContainer.class);

    public ElevatorContainer(int elevatorCapacity, int storiesNumber) {
        this.elevatorCapacity = elevatorCapacity;
        this.passengersList = new ArrayList<Passenger>(elevatorCapacity);
        this.currentStory = 1;
        this.direction = Direction.UP;
        this.storiesNumber = storiesNumber;
    }

    public List<Passenger> getPassengersList() {
        return passengersList;
    }

    public int getCurrentStory() {
        return currentStory;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setCurrentStory(int currentStory) {
        this.currentStory = currentStory;
    }

    /**
     * add passengers to elevator
     *
     * @param passenger value
     */
    public void boardingPassengers(Passenger passenger) {
        logger.info(TransportationActions.BOARDING_OF_PASSENGER + " " + passenger.getPassengerID());
        passengersList.add(passenger);
    }

    /**
     * deboarding passengers from elevator
     *
     * @param passenger value
     */
    public void deboardingPassengers(Passenger passenger) {
        logger.info(TransportationActions.DEBOARDING_OF_PASSENGER + " " + passenger.getPassengerID());
        passengersList.remove(passenger);
    }

    /**
     * this method notifies all passengers in elevator
     */
    public synchronized void notifyElevatorPassengers() {
        notifyAll();
    }

    public synchronized void waitInElevator(){
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "ElevatorContainer{" +
                "passengersList=" + passengersList +
                ", currentStory=" + currentStory +
                ", elevatorCapacity=" + elevatorCapacity +
                '}';
    }

    public void moveUp() {
        if (currentStory < storiesNumber) {
            logger.info(TransportationActions.MOVING_ELEVATOR + " from story " + currentStory + " to " + (currentStory + 1));
            currentStory++;
        }
    }

    public void moveDown() {
        if (currentStory > 1) {
            logger.info(TransportationActions.MOVING_ELEVATOR + " from story " + currentStory + " to " + (currentStory - 1));
            currentStory--;
        }
    }

    public boolean isEnter() {
        return passengersList.size() < elevatorCapacity;
    }
}
