package beans;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class StoryTest {

    private Story story;

    @Before
    public void initialize(){
        story = new Story();
    }

    @Test
    public void testGetStoryPassengers() {
        assertEquals(0, story.getStoryPassengers().size());
    }

    @Test
    public void testAddPassenger() {
        Passenger passenger = mock(Passenger.class);
        story.addPassenger(passenger);
        assertEquals(1, story.getStoryPassengers().size());
    }

    @Test
    public void removePassenger() {
        Passenger passenger = mock(Passenger.class);
        story.addPassenger(passenger);
        story.removePassenger(passenger);
        assertEquals(0, story.getStoryPassengers().size());
    }
}