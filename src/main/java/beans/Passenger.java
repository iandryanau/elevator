package beans;

import enums.TransportationState;

/**
 * this class describes passenger
 */
public class Passenger {

    private int destination;
    private int passengerID;
    private int start;
    private TransportationState transportationState;

    public Passenger(int destination, int passengerID, int start) {
        this.destination = destination;
        this.passengerID = passengerID;
        this.start = start;
        this.transportationState = TransportationState.NOT_STARTED;
    }

    public int getdestination() {
        return destination;
    }

    public int getPassengerID() {
        return passengerID;
    }

    public int getstart() {
        return start;
    }

    /**
     * when first run must be not started
     *
     * @return transportationState value
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }

    public void setTransportationState(TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "start=" + start +
                ", PASSENGER ID=" + passengerID +
                ", destination=" + destination +
                ", transportationState=" + transportationState +
                '}';
    }
}
