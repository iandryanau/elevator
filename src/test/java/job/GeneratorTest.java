package job;

import beans.Passenger;
import beans.Story;
import job.Generator;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class GeneratorTest {

    @Test
    public void testIsStoriesNumberIsTen() {
        Generator generator = new Generator("config");
        assertEquals(10, generator.getStoriesNumber());
    }

    @Test
    public void testIsElevatorCapacityIsFive() {
        Generator generator = new Generator("config");
        assertEquals(5, generator.getElevatorCapacity());
    }

    @Test
    public void testIsPassengersNumberIs20() {
        Generator generator = new Generator("config");
        assertEquals(20, generator.getPassengersNumber());
    }

    @Test
    public void testIsPassengersAreGenerated() {
        Generator generator = new Generator("config");
        List<Passenger> passengerList = generator.generatePassengers();
        assertEquals(20, passengerList.size());
    }

    @Test
    public void testIsDispatchContainersAreGenerated() {
        Generator generator = new Generator("config");
        List<Passenger> passengerList = generator.generatePassengers();
        List<Story> stories = generator.generateDispatchContainers(passengerList);
        assertEquals(10, stories.size());
    }

    @Test
    public void testIsArrivalContainersAreGenerated() {
        Generator generator = new Generator("config");
        assertEquals(10, generator.generateArrivalContainers().size());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testWrongParametersFromFile(){
        Generator generator = new Generator("WrongConfig");
        generator.getPassengersNumber();
    }
}