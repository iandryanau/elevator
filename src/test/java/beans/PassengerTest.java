package beans;

import enums.TransportationState;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PassengerTest {

    @Test
    public void getDestinationStory() {
        Passenger passenger = new Passenger(5, 0, 1);
        assertEquals(5, passenger.getDestinationStory());
    }

    @Test
    public void getPassengerID() {
        Passenger passenger = new Passenger(5, 9, 1);
        assertEquals(9, passenger.getPassengerID());
    }

    @Test
    public void getStartStory() {
        Passenger passenger = new Passenger(5, 0, 1);
        assertEquals(1, passenger.getStartStory());
    }

    @Test
    public void getTransportationState() {
        Passenger passenger = new Passenger(5, 0, 1);
        assertEquals(TransportationState.NOT_STARTED, passenger.getTransportationState());
    }

    @Test
    public void setTransportationState() {
        Passenger passenger = new Passenger(5, 0, 1);
        passenger.setTransportationState(TransportationState.COMPLETED);
        assertEquals(TransportationState.COMPLETED, passenger.getTransportationState());
    }
}