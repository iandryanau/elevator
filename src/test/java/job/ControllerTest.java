package job;

import beans.Building;
import beans.Passenger;
import org.junit.Before;
import org.junit.Test;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ControllerTest {

    private Generator generator;
    private Controller controller;
    private Building building;

    @Before
    public void getInitialized() {
        generator = new Generator("config");
        building = new Building(generator);
        controller = new Controller(building);
    }

    @Test
    public void testIsPassengerCanEnterToElevator() {
        Passenger passenger = mock(Passenger.class);
        when(passenger.getStartStory()).thenReturn(1);

        assertTrue(controller.isPassengerCanEnter(passenger));

    }

    @Test
    public void testIsPassengerCanLeaveElevator() {
        Passenger passenger = mock(Passenger.class);
        when(passenger.getDestinationStory()).thenReturn(1);

        assertTrue(controller.isPassengerCanLeave(passenger));
    }

    @Test
    public void testIsElevatorMoves() {
        assertEquals(1, building.getElevatorContainer().getCurrentStory());
        for (int i = 0; i < 10; i++) {
            controller.moveElevator();
        }
        assertEquals(10, building.getElevatorContainer().getCurrentStory());
        for (int i = 0; i < 2; i++) {
            controller.moveElevator();
        }
        assertEquals(8, building.getElevatorContainer().getCurrentStory());


    }
}