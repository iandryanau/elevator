package job;

import beans.Building;
import beans.Passenger;
import enums.TransportationState;
import job.Controller;

/**
 * this class describes a transportation task for each passenger
 */
public class TransportationTask implements Runnable {

    private Passenger passenger;
    private Building building;
    private Controller controller;

    public TransportationTask(Passenger passenger, Building building, Controller controller) {
        this.passenger = passenger;
        this.building = building;
        this.controller = controller;
    }

    public void run() {
        do {
            building.getDispatchStory(passenger.getStartStory()-1).waitOnTheStory();
        } while (!controller.isPassengerCanEnter(passenger));
        passenger.setTransportationState(TransportationState.IN_PROGRESS);

        do {
            building.getElevatorContainer().waitInElevator();
        } while (!controller.isPassengerCanLeave(passenger));
        passenger.setTransportationState(TransportationState.COMPLETED);

    }
}
