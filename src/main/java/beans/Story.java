package beans;

import java.util.ArrayList;
import java.util.List;

public class Story {

    private List<Passenger> passengerList;

    /**
     * specifying custom story with passengers
     */
    public Story() {
        this.passengerList = new ArrayList<Passenger>();
    }

    /**
     * this method returns all passengers from this story
     *
     * @return
     */
    public List<Passenger> getStoryPassengers() {
        return passengerList;
    }

    /**
     * this method adds passengers to this story
     *
     * @param passenger
     */
    public void addPassenger(Passenger passenger) {
        passengerList.add(passenger);
    }

    /**
     * this method remove passengers from this story
     *
     * @param passenger
     */
    public void removePassenger(Passenger passenger) {
        passengerList.remove(passenger);
    }

    public synchronized void waitOnTheStory() {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * notify all passengers
     */
    public synchronized void notifyPassengers() {
        notifyAll();
    }

    @Override
    public String toString() {
        return "Story{" +
                "passengerList=" + passengerList +
                '}';
    }
}
