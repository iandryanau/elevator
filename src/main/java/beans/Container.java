package beans;

import java.util.List;

/**
 * Class Container.
 * Have dispatch and arrival containers, based on Story class
 */
public class Container {

    private List<Story> dispatchStoryContainer;
    private List<Story> arrivalStoryContainer;

    public Container(List<Story> dispatchStoryContainer, List<Story> arrivalStoryContainer) {
        this.dispatchStoryContainer = dispatchStoryContainer;
        this.arrivalStoryContainer = arrivalStoryContainer;
    }

    public List<Story> getDispatchStoryContainer() {
        return dispatchStoryContainer;
    }

    public List<Story> getArrivalStoryContainer() {
        return arrivalStoryContainer;
    }
}
