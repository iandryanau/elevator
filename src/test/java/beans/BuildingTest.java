package beans;

import enums.Direction;
import job.Generator;
import org.junit.Before;
import org.junit.Test;
import sun.nio.cs.Surrogate;

import java.awt.geom.GeneralPath;
import java.util.List;

import static org.junit.Assert.*;

public class BuildingTest {

    private Generator generator;
    private Building building;

    @Before
    public void getInitialized() {
        generator = new Generator("config");
        building = new Building(generator);
    }

    @Test
    public void testGetStoriesNumber() {
        assertEquals(10, building.getStoriesNumber());
    }

    @Test
    public void testGetGenerator() {
        assertEquals(generator, building.getGenerator());
    }

    @Test
    public void testAmountOfPassengers() {
        assertEquals(generator.generatePassengers().size(), building.getPassengersList().size());
    }

    @Test
    public void testElevatorContainer() {
        assertEquals(Direction.UP, building.getElevatorContainer().getDirection());
        building.getElevatorContainer().setDirection(Direction.DOWN);
        assertEquals(Direction.DOWN, building.getElevatorContainer().getDirection());
    }


    @Test
    public void getDispatchStory() {
        Story dispatch = building.getDispatchStory(0);
        System.out.println(dispatch.toString());
    }

    @Test
    public void getArrivalStory() {
        Story arrival = building.getArrivalStory(0);
        System.out.println(arrival.getStoryPassengers());
    }
}