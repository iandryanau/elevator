package beans;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class ElevatorContainerTest {

    @Test
    public void testPassengersListSize() {
        ElevatorContainer elevatorContainer = new ElevatorContainer(5, 10);
        assertEquals(0, elevatorContainer.getPassengersList().size());
    }

    @Test
    public void testGetCurrentStory() {
        ElevatorContainer elevatorContainer = new ElevatorContainer(5, 10);
        assertEquals(1, elevatorContainer.getCurrentStory());
    }

    @Test
    public void testBoardingPassengers() {
        Passenger passenger = mock(Passenger.class);
        ElevatorContainer elevatorContainer = new ElevatorContainer(5, 10);
        elevatorContainer.boardingPassengers(passenger);
        assertEquals(1, elevatorContainer.getPassengersList().size());
    }

    @Test
    public void testDeboardingPassengers() {
        Passenger passenger = mock(Passenger.class);
        ElevatorContainer elevatorContainer = new ElevatorContainer(5, 10);
        elevatorContainer.boardingPassengers(passenger);
        elevatorContainer.deboardingPassengers(passenger);
        assertEquals(0, elevatorContainer.getPassengersList().size());
    }

    @Test
    public void testMoveUp() {
        ElevatorContainer elevatorContainer = new ElevatorContainer(5,10);
        elevatorContainer.moveUp();
        assertEquals(2, elevatorContainer.getCurrentStory());
    }

    @Test
    public void testMoveDown() {
        ElevatorContainer elevatorContainer = new ElevatorContainer(5,10);
        elevatorContainer.setCurrentStory(10);
        elevatorContainer.moveDown();
        assertEquals(9, elevatorContainer.getCurrentStory());
    }

    @Test
    public void testIsEnter() {
        ElevatorContainer elevatorContainer = new ElevatorContainer(5,10);
        assertTrue(elevatorContainer.isEnter());
    }
}