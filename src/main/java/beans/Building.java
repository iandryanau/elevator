package beans;

import java.util.List;

import job.Generator;

/**
 * class of the building.
 * it has elevator object,
 * all passengers and containers
 */
public class Building {

    private int storiesNumber;
    private Generator generator;
    private List<Passenger> passengersList;
    private ElevatorContainer elevatorContainer;
    private Container container;

    /**
     * constructor.
     *
     * @param generator object
     */
    public Building(Generator generator) {
        this.generator = generator;
        this.storiesNumber = generator.getStoriesNumber();
        passengersList = generator.generatePassengers();
        elevatorContainer = new ElevatorContainer(generator.getElevatorCapacity(),
                storiesNumber);
        container = new Container(generator.generateDispatchContainers(passengersList),
                generator.generateArrivalContainers());
    }

    public int getStoriesNumber() {
        return storiesNumber;
    }

    public Generator getGenerator() {
        return generator;
    }

    public List<Passenger> getPassengersList() {
        return passengersList;
    }

    public ElevatorContainer getElevatorContainer() {
        return elevatorContainer;
    }

    public Story getDispatchStory(final int story) {
        return container.getDispatchStoryContainer().get(story);
    }

    public Story getArrivalStory(final int story) {
        return container.getArrivalStoryContainer().get(story);
    }


}
