Project consists of 12 classes:

3 enums:
 - Direction
 - TrasportationState
 - TransportationActions

5 beans:
 - Building (class for building structure of the task, have containers and passengers inside)
 - Container (have dispatch and arrival containers inside)
 - ElevatorContainer (elevator; can board or deboard passengers)
 - Story (story with passengers)
 - Passenger (have each passenger information inside)

3 job classes:
 - Generator (generates all needing information from config file)
 - Controller (moves elevator up and down and checks, if passengers can enter or leave elevator)
 - TransportationTask (tells passengers what to do)

Runner runs the program.

In this project the org.mockito 1.9.5 version, junit release version and log4j 1.2.17 version were used by dependencies in maven.