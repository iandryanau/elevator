package job;

import beans.Building;
import beans.Passenger;
import enums.Direction;
import enums.TransportationActions;
import enums.TransportationState;
import org.apache.log4j.Logger;

/**
 * this class describes an operator that control and move elevator
 */
public class Controller implements Runnable {

    private Logger logger = Logger.getLogger(Controller.class);
    private Building building;

    public Controller(Building building) {
        this.building = building;
        logger.info(TransportationActions.STARTING_TRANSPORTATION);
    }


    /**
     * this method moves elevator up and down in building
     */
    public void moveElevator() {
        if (Direction.UP.equals(building.getElevatorContainer().getDirection())) {
            if (building.getElevatorContainer().getCurrentStory() < building.getStoriesNumber()) {
                building.getElevatorContainer().moveUp();
            } else {
                building.getElevatorContainer().setDirection(Direction.DOWN);
            }
        } else {
            if (building.getElevatorContainer().getCurrentStory() > 1) {
                building.getElevatorContainer().moveDown();
            } else
                building.getElevatorContainer().setDirection(Direction.UP);
        }
    }

    public void run() {
        try {
            while (!isFinished()) {
                building.getElevatorContainer().notifyElevatorPassengers();
                Thread.sleep(200);

                building.getDispatchStory(building.getElevatorContainer().getCurrentStory() - 1).notifyPassengers();

                Thread.sleep(200);

                moveElevator();
            }
        } catch (InterruptedException e) {
            logger.debug("Thread was interrupted in Controller" + e);
        }
    }

    private boolean isFinished() {

        boolean isFinished = false;

        int dispatchContainerSize = 0;
        int arrivalContainerSize = 0;
        int elevatorNumber = building.getElevatorContainer().getPassengersList().size();
        for (int i = 0; i < building.getGenerator().getStoriesNumber(); i++) {
            dispatchContainerSize += building.getDispatchStory(i).getStoryPassengers().size();
        }

        for (int i = 0; i < building.getGenerator().getStoriesNumber(); i++) {
            for (int j = 0; j < building.getArrivalStory(i).getStoryPassengers().size(); j++) {
                Passenger passenger = building.getArrivalStory(i).getStoryPassengers().get(j);
                if (passenger.getDestinationStory() == i + 1
                        && TransportationState.COMPLETED.equals(passenger.getTransportationState())) {
                    arrivalContainerSize++;
                }
            }
        }

        if (arrivalContainerSize == building.getPassengersList().size()
                && dispatchContainerSize == 0 && elevatorNumber == 0) {
            isFinished = true;
            logger.info(TransportationActions.COMPLETION_TRANSPORTATION);
        }

        return isFinished;
    }

    /**
     * this method tells passenger is he can enter to elevator
     *
     * @return
     */
    public synchronized boolean isPassengerCanEnter(Passenger passenger) {
        boolean isPassengerCanEnter = false;
        if (building.getElevatorContainer().isEnter()
                && building.getElevatorContainer().getCurrentStory() == passenger.getStartStory()) {
            building.getDispatchStory(building.getElevatorContainer().getCurrentStory() - 1).removePassenger(passenger);
            building.getElevatorContainer().boardingPassengers(passenger);
            isPassengerCanEnter = true;
        }
        return isPassengerCanEnter;
    }

    /**
     * this method tells passenger is he can leave elevator
     *
     * @return
     */
    public synchronized boolean isPassengerCanLeave(Passenger passenger) {
        boolean isCanLeave = false;
        if (passenger.getDestinationStory() == building.getElevatorContainer().getCurrentStory()) {
            building.getElevatorContainer().deboardingPassengers(passenger);
            building.getArrivalStory(building.getElevatorContainer().getCurrentStory() - 1).addPassenger(passenger);
            isCanLeave = true;
        }

        return isCanLeave;
    }
}
