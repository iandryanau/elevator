package job;

import beans.Passenger;
import beans.Story;
import org.apache.log4j.Logger;

import java.security.SecureRandom;
import java.util.*;

/**
 * this class reads data from config.property and generate passengers, containers, etc.
 */
public class Generator {

    private Logger logger = Logger.getLogger(Generator.class);

    private int storiesNumber;
    private int elevatorCapacity;
    private int passengersNumber;

    public Generator(String filename) {
        readPropertiesFromConfig(filename);
    }

    public int getStoriesNumber() {
        return storiesNumber;
    }

    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    public int getPassengersNumber() {
        return passengersNumber;
    }

    /**
     * this method puts passengers to their floors in random order
     *
     * @return passengers
     */
    public List<Passenger> generatePassengers() {
        int destinationStory;
        int initialStory;

        List<Passenger> passengers = new ArrayList<Passenger>();
        SecureRandom random = new SecureRandom();
        Passenger passenger;

        for (int i = 0; i < passengersNumber; i++) {
            initialStory = random.nextInt(storiesNumber) + 1;
            destinationStory = random.nextInt(storiesNumber) + 1;
            while (destinationStory == initialStory) {
                destinationStory = random.nextInt(storiesNumber) + 1;
            }
            passenger = new Passenger(destinationStory, i, initialStory);
            passengers.add(passenger);
            logger.info(passenger.toString());
        }

        return passengers;
    }

    /**
     * this method generates dispatch containers that store generated passengers
     *
     * @return list values
     */
    public List<Story> generateDispatchContainers(List<Passenger> passengers) {
        List<Story> dispatchContainers = new ArrayList<Story>(storiesNumber);
        for (int i = 0; i < storiesNumber; i++) {
            Story story = new Story();
            dispatchContainers.add(story);
        }
        logger.info("GENERATE DISPATCH CONTAINERS");
        for (Passenger passenger : passengers) {
            dispatchContainers.get(passenger.getStartStory()-1).addPassenger(passenger);
        }
        return dispatchContainers;
    }

    /**
     * this method generates arrival containers that store arrived passengers
     *
     * @return list
     */
    public List<Story> generateArrivalContainers() {
        List<Story> arrivalContainers = new ArrayList<Story>(storiesNumber);
        for (int i = 0; i < storiesNumber; i++) {
            Story story = new Story();
            arrivalContainers.add(story);
        }
        logger.info("GENERATE ARRIVAL CONTAINERS");
        return arrivalContainers;
    }

    /**
     * this method reads properties from config file
     *
     * @param fileName consists of three properties:
     *                 storiesNumber=10
     *                 elevatorCapacity=5
     *                 passengersNumber=200
     */
    private void readPropertiesFromConfig(String fileName) {
        ResourceBundle myResources = ResourceBundle.getBundle(fileName, Locale.getDefault());
        storiesNumber = Integer.parseInt(myResources.getString("storiesNumber"));
        if (storiesNumber < 2) {
            logger.debug("readPropertiesFromFile mistake");
            throw new IllegalArgumentException("stories must be greater or equal to 2");
        }
        elevatorCapacity = Integer.parseInt(myResources.getString("elevatorCapacity"));
        if (elevatorCapacity < 1) {
            logger.debug("readPropertiesFromFile mistake");
            throw new IllegalArgumentException("elevatorCapacity must be greater or equal to 1");
        }
        passengersNumber = Integer.parseInt(myResources.getString("passengersNumber"));
        if (passengersNumber < 1) {
            logger.debug("readPropertiesFromFile mistake");
            throw new IllegalArgumentException("passengersNumber must be greater or equal to 1");
        }
    }
}
